package api.ferreteria.service;

import api.ferreteria.modelo.Estadooc;
import api.ferreteria.modelo.Familia;
import java.util.Collection;
import java.util.List;
import javax.inject.Inject;
import javax.persistence.EntityManager;
import javax.transaction.Transactional;
import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.PATCH;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;

@Path("/estadooc")
public class MSEstadooc {
    
    @Inject EntityManager manager;
    
    @GET
    @Produces(MediaType.APPLICATION_JSON)
    public Collection<Estadooc> getEstadosoc(){
        String query = "SELECT F FROM ESTADOOC F ORDER BY F.nomestadooc ASC";
        return manager.createQuery(query, Estadooc.class).getResultList();
    }
    
    @GET
    @Path("/{idestadooc}")
    @Produces(MediaType.APPLICATION_JSON)
    public Estadooc getEstadooc(@PathParam("idestadooc") Integer idestadooc){
        String query = "SELECT F FROM ESTADOOC F WHERE F.idestadooc = " + idestadooc;
        List<Estadooc> estadosoc = manager.createQuery(query, Estadooc.class).getResultList();
        if(estadosoc.size() < 1){
            return new Estadooc();
        }
        return estadosoc.get(0); 
    }
    
    @POST 
    @Produces(MediaType.APPLICATION_JSON)
    @Consumes(MediaType.APPLICATION_JSON)
    @Transactional
    public Estadooc postEstadooc(Estadooc estadooc){
        manager.persist(estadooc);
        manager.flush(); 
        return manager.find(Estadooc.class, estadooc.getIdestadooc());
    }
    
    @PATCH
    @Produces(MediaType.APPLICATION_JSON)
    @Consumes(MediaType.APPLICATION_JSON)
    @Transactional
    public Estadooc patchEstadooc(Estadooc estadooc){
        manager.merge(estadooc);
        manager.flush();
        return manager.find(Estadooc.class, estadooc.getIdestadooc());
    }
}
