package api.ferreteria.service;

import api.ferreteria.modelo.Clienteempresa;
import java.util.Collection;
import java.util.List;
import javax.inject.Inject;
import javax.persistence.EntityManager;
import javax.transaction.Transactional;
import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.PATCH;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;

@Path("/clienteempresa")
public class MSClienteempresa {
    
    @Inject EntityManager manager;
    
    @GET
    @Produces(MediaType.APPLICATION_JSON)
    public Collection<Clienteempresa> getClientesempresa(){
        String query = "SELECT F FROM CLIENTEEMPRESA F ORDER BY F.nomclienteempresa ASC";
        return manager.createQuery(query, Clienteempresa.class).getResultList();
    }
    
    @GET
    @Path("/{idclienteempresa}")
    @Produces(MediaType.APPLICATION_JSON)
    public Clienteempresa getClienteempresa(@PathParam("idclienteempresa") Integer idclienteempresa){
        String query = "SELECT F FROM CLIENTEEMPRESA F WHERE F.idclienteempresa = " + idclienteempresa;
        List<Clienteempresa> clientesempresa = manager.createQuery(query, Clienteempresa.class).getResultList();
        if(clientesempresa.size() < 1){
            return new Clienteempresa();
        }
        return clientesempresa.get(0); 
    }
    
    @POST 
    @Produces(MediaType.APPLICATION_JSON)
    @Consumes(MediaType.APPLICATION_JSON)
    @Transactional
    public Clienteempresa postClienteempresa(Clienteempresa clienteempresa){
        manager.persist(clienteempresa);
        manager.flush(); 
        return manager.find(Clienteempresa.class, clienteempresa.getIdclienteempresa());
    }
    
    @PATCH
    @Produces(MediaType.APPLICATION_JSON)
    @Consumes(MediaType.APPLICATION_JSON)
    @Transactional
    public Clienteempresa patchClienteempresa(Clienteempresa clienteempresa){
        manager.merge(clienteempresa);
        manager.flush();
        return manager.find(Clienteempresa.class, clienteempresa.getIdclienteempresa());
        
    }
}
