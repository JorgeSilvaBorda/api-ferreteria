package api.ferreteria.service;

import api.ferreteria.modelo.Estadodetalleoc;
import java.util.Collection;
import java.util.List;
import javax.inject.Inject;
import javax.persistence.EntityManager;
import javax.transaction.Transactional;
import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.PATCH;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;

@Path("/estadodetalleoc")
public class MSEstadodetalleoc {
    
    @Inject EntityManager manager;
    
    @GET
    @Produces(MediaType.APPLICATION_JSON)
    public Collection<Estadodetalleoc> getEstadosdetalleoc(){
        String query = "SELECT F FROM ESTADODETALLEOC F ORDER BY F.nomestadodetalleoc ASC";
        return manager.createQuery(query, Estadodetalleoc.class).getResultList();
    }
    
    @GET
    @Path("/{idestadodetalleoc}")
    @Produces(MediaType.APPLICATION_JSON)
    public Estadodetalleoc getEstadodetalleoc(@PathParam("idestadodetalleoc") Integer idestadodetalleoc){
        String query = "SELECT F FROM ESTADODETALLEOC F WHERE F.idestadodetalleoc = " + idestadodetalleoc;
        List<Estadodetalleoc> estadosdetalleoc = manager.createQuery(query, Estadodetalleoc.class).getResultList();
        if(estadosdetalleoc.size() < 1){
            return new Estadodetalleoc();
        }
        return estadosdetalleoc.get(0); 
    }
    
    @POST 
    @Produces(MediaType.APPLICATION_JSON)
    @Consumes(MediaType.APPLICATION_JSON)
    @Transactional
    public Estadodetalleoc postEstadodetalleoc(Estadodetalleoc estadodetalleoc){
        manager.persist(estadodetalleoc);
        manager.flush(); 
        return manager.find(Estadodetalleoc.class, estadodetalleoc.getIdestadodetalleoc());
    }
    
    @PATCH
    @Produces(MediaType.APPLICATION_JSON)
    @Consumes(MediaType.APPLICATION_JSON)
    @Transactional
    public Estadodetalleoc patchEstadodetalleoc(Estadodetalleoc estadodetalleoc){
        manager.merge(estadodetalleoc);
        manager.flush();
        return manager.find(Estadodetalleoc.class, estadodetalleoc.getIdestadodetalleoc());
    }
}
