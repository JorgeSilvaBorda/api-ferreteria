package api.ferreteria.service;

import api.ferreteria.modelo.Perfil;
import java.util.Collection;
import java.util.List;
import javax.inject.Inject;
import javax.persistence.EntityManager;
import javax.transaction.Transactional;
import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.PATCH;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;

@Path("/perfil")
public class MSPerfil{
    
    @Inject EntityManager manager;
    
    @GET
    @Produces(MediaType.APPLICATION_JSON)
    public Collection<Perfil> getPerfiles(){
        String query = "SELECT F FROM PERFIL F ORDER BY F.nomperfil ASC";
        return manager.createQuery(query, Perfil.class).getResultList();
    }
    
    @GET
    @Path("/{idperfil}")
    @Produces(MediaType.APPLICATION_JSON)
    public Perfil getPerfil(@PathParam("idperfil") Integer idperfil){
        String query = "SELECT F FROM PERFIL F WHERE F.idperfil = " + idperfil;
        List<Perfil> perfiles = manager.createQuery(query, Perfil.class).getResultList();
        if(perfiles.size() < 1){
            return new Perfil();
        }
        return perfiles.get(0); 
    }
    
    @POST 
    @Produces(MediaType.APPLICATION_JSON)
    @Consumes(MediaType.APPLICATION_JSON)
    @Transactional
    public Perfil postPerfil(Perfil perfil){
        manager.persist(perfil);
        manager.flush(); 
        return manager.find(Perfil.class, perfil.getIdperfil());
    }
    
    @PATCH
    @Produces(MediaType.APPLICATION_JSON)
    @Consumes(MediaType.APPLICATION_JSON)
    @Transactional
    public Perfil patchPerfil(Perfil perfil){
        manager.merge(perfil);
        manager.flush();
        return manager.find(Perfil.class, perfil.getIdperfil());
    }
}
