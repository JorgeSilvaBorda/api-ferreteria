package api.ferreteria.service;

import api.ferreteria.modelo.Cargo;
import api.ferreteria.modelo.Cliente;
import api.ferreteria.modelo.Familia;
import java.util.Collection;
import java.util.List;
import javax.inject.Inject;
import javax.persistence.EntityManager;
import javax.transaction.Transactional;
import javax.ws.rs.Consumes;
import javax.ws.rs.DELETE;
import javax.ws.rs.GET;
import javax.ws.rs.PATCH;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;

@Path("/cliente")
public class MSCliente {
    
    @Inject EntityManager manager;
    
    @GET
    @Produces(MediaType.APPLICATION_JSON)
    public Collection<Cliente> getClientes(){
        String query = "SELECT F FROM CLIENTE F ORDER BY F.nomcliente ASC";
        return manager.createQuery(query, Cliente.class).getResultList();
    }
    
    @GET
    @Path("/{idcliente}")
    @Produces(MediaType.APPLICATION_JSON)
    public Cliente getCliente(@PathParam("idcliente") Integer idcliente){
        String query = "SELECT F FROM CLIENTE F WHERE F.idcliente = " + idcliente;
        List<Cliente> clientes = manager.createQuery(query, Cliente.class).getResultList();
        if(clientes.size() < 1){
            return new Cliente();
        }
        return clientes.get(0); 
    }
    
    @POST 
    @Produces(MediaType.APPLICATION_JSON)
    @Consumes(MediaType.APPLICATION_JSON)
    @Transactional
    public Cliente postCliente(Cliente cliente){
        manager.persist(cliente);
        manager.flush(); 
        return manager.find(Cliente.class, cliente.getIdcliente());
    }
    
    @PATCH
    @Produces(MediaType.APPLICATION_JSON)
    @Consumes(MediaType.APPLICATION_JSON)
    @Transactional
    public Cliente patchCliente(Cliente cliente){
        manager.merge(cliente);
        manager.flush();
        return manager.find(Cliente.class, cliente.getIdcliente());
    }
    @DELETE
    @Produces(MediaType.APPLICATION_JSON)
    @Consumes(MediaType.APPLICATION_JSON)
    @Transactional
    public Cliente deleteCliente(Cliente cliente){
        Cliente f = manager.find(Cliente.class, cliente.getIdcliente());
        manager.remove(f);
        manager.flush();
        return new Cliente();
    }
}
