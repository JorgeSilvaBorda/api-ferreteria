package api.ferreteria.service;

import api.ferreteria.modelo.Moneda;
import java.util.Collection;
import java.util.List;
import javax.inject.Inject;
import javax.persistence.EntityManager;
import javax.transaction.Transactional;
import javax.ws.rs.Consumes;
import javax.ws.rs.DELETE;
import javax.ws.rs.GET;
import javax.ws.rs.PATCH;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;

@Path("/moneda")
public class MSMoneda {
    
    @Inject EntityManager manager;
    
    @GET
    @Produces(MediaType.APPLICATION_JSON)
    public Collection<Moneda> getMonedas(){
        String query = "SELECT F FROM MONEDA F ORDER BY F.nommoneda ASC";
        return manager.createQuery(query, Moneda.class).getResultList();
    }
    
    @GET
    @Path("/{idmoneda}")
    @Produces(MediaType.APPLICATION_JSON)
    public Moneda getMoneda(@PathParam("idmoneda") Integer idmoneda){
        String query = "SELECT F FROM MONEDA F WHERE F.idmoneda = " + idmoneda;
        List<Moneda> monedas = manager.createQuery(query, Moneda.class).getResultList();
        if(monedas.size() < 1){
            return new Moneda();
        }
        return monedas.get(0); 
    }
    
    @POST 
    @Produces(MediaType.APPLICATION_JSON)
    @Consumes(MediaType.APPLICATION_JSON)
    @Transactional
    public Moneda postMoneda(Moneda moneda){
        manager.persist(moneda);
        manager.flush(); 
        return manager.find(Moneda.class, moneda.getIdmoneda());
    }
    
    @PATCH
    @Produces(MediaType.APPLICATION_JSON)
    @Consumes(MediaType.APPLICATION_JSON)
    @Transactional
    public Moneda patchMoneda(Moneda moneda){
        manager.merge(moneda);
        manager.flush();
        return manager.find(Moneda.class, moneda.getIdmoneda());
    }
    
    @DELETE
    @Produces(MediaType.APPLICATION_JSON)
    @Consumes(MediaType.APPLICATION_JSON)
    @Transactional
    public Moneda deleteMoneda(Moneda moneda){
        Moneda f = manager.find(Moneda.class, moneda.getIdmoneda());
        manager.remove(f);
        manager.flush();
        return new Moneda();
    }
}
