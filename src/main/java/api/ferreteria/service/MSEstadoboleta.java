package api.ferreteria.service;

import api.ferreteria.modelo.Estadoboleta;
import java.util.Collection;
import java.util.List;
import javax.inject.Inject;
import javax.persistence.EntityManager;
import javax.transaction.Transactional;
import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.PATCH;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;

@Path("/estadoboleta")
public class MSEstadoboleta {
    
    @Inject EntityManager manager;
    
    @GET
    @Produces(MediaType.APPLICATION_JSON)
    public Collection<Estadoboleta> getEstadoboletas(){
        String query = "SELECT F FROM ESTADOBOLETA F ORDER BY F.nomestadoboleta ASC";
        return manager.createQuery(query, Estadoboleta.class).getResultList();
    }
    
    @GET
    @Path("/{idestadoboleta}")
    @Produces(MediaType.APPLICATION_JSON)
    public Estadoboleta getEstadoboleta(@PathParam("idestadoboleta") Integer idestadoboleta){
        String query = "SELECT F FROM ESTADOBOLETA F WHERE F.idestadoboleta = " + idestadoboleta;
        List<Estadoboleta> estadosboleta = manager.createQuery(query, Estadoboleta.class).getResultList();
        if(estadosboleta.size() < 1){
            return new Estadoboleta();
        }
        return estadosboleta.get(0); 
    }
    
    @POST 
    @Produces(MediaType.APPLICATION_JSON)
    @Consumes(MediaType.APPLICATION_JSON)
    @Transactional
    public Estadoboleta postEstadoboleta(Estadoboleta estadoboleta){
        manager.persist(estadoboleta);
        manager.flush(); 
        return manager.find(Estadoboleta.class, estadoboleta.getIdestadoboleta());
    }
    
    @PATCH
    @Produces(MediaType.APPLICATION_JSON)
    @Consumes(MediaType.APPLICATION_JSON)
    @Transactional
    public Estadoboleta patchEstadoboleta(Estadoboleta estadoboleta){
        manager.merge(estadoboleta);
        manager.flush();
        return manager.find(Estadoboleta.class, estadoboleta.getIdestadoboleta());
    }
}
