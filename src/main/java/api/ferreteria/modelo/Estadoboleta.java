/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package api.ferreteria.modelo;

import java.io.Serializable;
import javax.enterprise.context.ApplicationScoped;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;

@ApplicationScoped
@Entity(name = "ESTADOBOLETA")
public class Estadoboleta implements Serializable{
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "IDESTADOBOLETA")
    Integer idestadoboleta;
    
    @Column(name = "NOMESTADOBOLETA")
    String nomestadoboleta;

    public Estadoboleta() {
    }

    public Estadoboleta(Integer idestadoboleta, String nomestadoboleta) {
        this.idestadoboleta = idestadoboleta;
        this.nomestadoboleta = nomestadoboleta;
    }

    public Integer getIdestadoboleta() {
        return idestadoboleta;
    }

    public void setIdestadoboleta(Integer idestadoboleta) {
        this.idestadoboleta = idestadoboleta;
    }

    public String getNomestadoboleta() {
        return nomestadoboleta;
    }

    public void setNomestadoboleta(String nomestadoboleta) {
        this.nomestadoboleta = nomestadoboleta;
    }

    
    
    
}
