package api.ferreteria.modelo;

import java.io.Serializable;
import javax.enterprise.context.ApplicationScoped;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;

@ApplicationScoped
@Entity(name = "FACTURA")
public class Factura implements Serializable{
    
    
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "IDFACTURA")
    Integer idfactura;
    
    @Id
    @ManyToOne(optional = false, cascade = CascadeType.MERGE, fetch = FetchType.LAZY)
    @JoinColumn(name = "IDEMPRESA")
    Empresa empresa;
    
    @Id        
    @ManyToOne(optional = false, cascade = CascadeType.MERGE, fetch = FetchType.LAZY)
    @JoinColumn(name = "IDESTADOFACTURA")
    Estadofactura estadofactura;
    
    @Id        
    @ManyToOne(optional = false, cascade = CascadeType.MERGE, fetch = FetchType.LAZY)
    @JoinColumn(name = "IDEMPELADO")
    Empleado empleado;
    
    @Column(name = "FECHAESTADO")
    String fechaestado;
    
    @Column(name = "TOTALNETO")
    Integer totalneto;
    
    @Column(name = "TOTALIVA")
    Integer totaliva;
    
    @Column(name = "TOTALBRUTO")
    Integer totalbruto;

    public Factura() {
    }

    public Factura(Integer idfactura, Empresa empresa, Estadofactura estadofactura, Empleado empleado, String fechaestado, Integer totalneto, Integer totaliva, Integer totalbruto) {
        this.idfactura = idfactura;
        this.empresa = empresa;
        this.estadofactura = estadofactura;
        this.empleado = empleado;
        this.fechaestado = fechaestado;
        this.totalneto = totalneto;
        this.totaliva = totaliva;
        this.totalbruto = totalbruto;
    }

    public Integer getIdfactura() {
        return idfactura;
    }

    public void setIdfactura(Integer idfactura) {
        this.idfactura = idfactura;
    }

    public Empresa getEmpresa() {
        return empresa;
    }

    public void setEmpresa(Empresa empresa) {
        this.empresa = empresa;
    }

    public Estadofactura getEstadofactura() {
        return estadofactura;
    }

    public void setEstadofactura(Estadofactura estadofactura) {
        this.estadofactura = estadofactura;
    }

    public Empleado getEmpleado() {
        return empleado;
    }

    public void setEmpleado(Empleado empleado) {
        this.empleado = empleado;
    }

    public String getFechaestado() {
        return fechaestado;
    }

    public void setFechaestado(String fechaestado) {
        this.fechaestado = fechaestado;
    }

    public Integer getTotalneto() {
        return totalneto;
    }

    public void setTotalneto(Integer totalneto) {
        this.totalneto = totalneto;
    }

    public Integer getTotaliva() {
        return totaliva;
    }

    public void setTotaliva(Integer totaliva) {
        this.totaliva = totaliva;
    }

    public Integer getTotalbruto() {
        return totalbruto;
    }

    public void setTotalbruto(Integer totalbruto) {
        this.totalbruto = totalbruto;
    }

    
    
    
}
