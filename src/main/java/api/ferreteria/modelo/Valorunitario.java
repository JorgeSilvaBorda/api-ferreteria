
package api.ferreteria.modelo;

import java.io.Serializable;
import javax.enterprise.context.ApplicationScoped;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;


@ApplicationScoped
@Entity(name = "VALORUNITARIO")
public class Valorunitario implements Serializable{
    
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "IDVALORUNITARIO")
    int idvalorunitario;
    
    @ManyToOne(optional = false, cascade = CascadeType.MERGE, fetch = FetchType.LAZY)
    @JoinColumn(name = "IDMONEDA")
    Moneda moneda;
    
    @ManyToOne(optional = false, cascade = CascadeType.MERGE, fetch = FetchType.LAZY)
    @JoinColumn(name = "IDSTOCKPRODUCTO")
    Producto stockproducto;
    
    @Column(name = "CANTMONEDA")
    Float cantmoneda;

    public Valorunitario() {
    }

    public Valorunitario(int idvalorunitario, Moneda moneda, Producto stockproducto, Float cantmoneda) {
        this.idvalorunitario = idvalorunitario;
        this.moneda = moneda;
        this.stockproducto = stockproducto;
        this.cantmoneda = cantmoneda;
    }

    public int getIdvalorunitario() {
        return idvalorunitario;
    }

    public void setIdvalorunitario(int idvalorunitario) {
        this.idvalorunitario = idvalorunitario;
    }

    public Moneda getMoneda() {
        return moneda;
    }

    public void setMoneda(Moneda moneda) {
        this.moneda = moneda;
    }

    public Producto getStockproducto() {
        return stockproducto;
    }

    public void setStockproducto(Producto stockproducto) {
        this.stockproducto = stockproducto;
    }

    public Float getCantmoneda() {
        return cantmoneda;
    }

    public void setCantmoneda(Float cantmoneda) {
        this.cantmoneda = cantmoneda;
    }
    
    
    
    
}
