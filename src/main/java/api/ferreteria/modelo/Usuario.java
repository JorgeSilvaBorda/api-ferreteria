
package api.ferreteria.modelo;

import java.io.Serializable;
import javax.enterprise.context.ApplicationScoped;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;


@ApplicationScoped
@Entity(name = "USUARIO")
public class Usuario implements Serializable{
    
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "IDUSUARIO")
    Integer idusuario;
    
    @Column(name = "RUTUSUARIO")
    String rutusuario;
    
    @Column(name = "CLAVEUSUARIO")
    String claveusuario;

    public Usuario() {
    }

    public Usuario(Integer idusuario, String rutusuario, String claveusuario) {
        this.idusuario = idusuario;
        this.rutusuario = rutusuario;
        this.claveusuario = claveusuario;
    }

    public Integer getIdusuario() {
        return idusuario;
    }

    public void setIdusuario(Integer idusuario) {
        this.idusuario = idusuario;
    }

    public String getRutusuario() {
        return rutusuario;
    }

    public void setRutusuario(String rutusuario) {
        this.rutusuario = rutusuario;
    }

    public String getClaveusuario() {
        return claveusuario;
    }

    public void setClaveusuario(String claveusuario) {
        this.claveusuario = claveusuario;
    }
    
    
    
    
    
}
