/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package api.ferreteria.modelo;

import java.io.Serializable;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.FetchType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;


public class Detallefactura implements Serializable{
    
    @Id
    @ManyToOne(optional = false, cascade = CascadeType.MERGE, fetch = FetchType.LAZY)
    @JoinColumn(name = "IDFACTURA")
    Factura iddetallefactura;
    
    @Id        
    @ManyToOne(optional = false, cascade = CascadeType.MERGE, fetch = FetchType.LAZY)
    @JoinColumn(name = "IDESTADODETALLEFACTURA")
    Estadodetallefactura estadodetallefactura;
    
    @Id        
    @ManyToOne(optional = false, cascade = CascadeType.MERGE, fetch = FetchType.LAZY)
    @JoinColumn(name = "IDEMPELADO")
    Empleado empleado;
    
    @Column(name = "CANTIDAD")
    Integer cantidad;
    
    @Column(name = "SUBTOTALNETO")
    Integer subtotalneto;
    
    @Column(name = "SUBTOTALIVA")
    Integer subtotaliva;
    
    @Column(name = "SUBTOTALBRUTO")
    Integer sybtotalbruto;
    
    @Column(name = "FECHAESTADO")
    String fechaestado;

    public Detallefactura() {
    }

    public Detallefactura(Factura iddetallefactura, Estadodetallefactura estadodetallefactura, Empleado empleado, Integer cantidad, Integer subtotalneto, Integer subtotaliva, Integer sybtotalbruto, String fechaestado) {
        this.iddetallefactura = iddetallefactura;
        this.estadodetallefactura = estadodetallefactura;
        this.empleado = empleado;
        this.cantidad = cantidad;
        this.subtotalneto = subtotalneto;
        this.subtotaliva = subtotaliva;
        this.sybtotalbruto = sybtotalbruto;
        this.fechaestado = fechaestado;
    }

    public Factura getIddetallefactura() {
        return iddetallefactura;
    }

    public void setIddetallefactura(Factura iddetallefactura) {
        this.iddetallefactura = iddetallefactura;
    }

    public Estadodetallefactura getEstadodetallefactura() {
        return estadodetallefactura;
    }

    public void setEstadodetallefactura(Estadodetallefactura estadodetallefactura) {
        this.estadodetallefactura = estadodetallefactura;
    }

    public Empleado getEmpleado() {
        return empleado;
    }

    public void setEmpleado(Empleado empleado) {
        this.empleado = empleado;
    }

    public Integer getCantidad() {
        return cantidad;
    }

    public void setCantidad(Integer cantidad) {
        this.cantidad = cantidad;
    }

    public Integer getSubtotalneto() {
        return subtotalneto;
    }

    public void setSubtotalneto(Integer subtotalneto) {
        this.subtotalneto = subtotalneto;
    }

    public Integer getSubtotaliva() {
        return subtotaliva;
    }

    public void setSubtotaliva(Integer subtotaliva) {
        this.subtotaliva = subtotaliva;
    }

    public Integer getSybtotalbruto() {
        return sybtotalbruto;
    }

    public void setSybtotalbruto(Integer sybtotalbruto) {
        this.sybtotalbruto = sybtotalbruto;
    }

    public String getFechaestado() {
        return fechaestado;
    }

    public void setFechaestado(String fechaestado) {
        this.fechaestado = fechaestado;
    }
    
    
    

   
    
    
}
