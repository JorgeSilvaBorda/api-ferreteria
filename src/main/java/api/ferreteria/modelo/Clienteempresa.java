package api.ferreteria.modelo;

import java.io.Serializable;
import javax.enterprise.context.ApplicationScoped;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;

@ApplicationScoped
@Entity(name = "CLIENTEEMPRESA")
public class Clienteempresa implements Serializable {

    @Id
    @Column(name = "IDCLIENTEEMPRESA")
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    int idclienteempresa;

    @Id
    @ManyToOne(optional = false, cascade = CascadeType.MERGE, fetch = FetchType.LAZY)
    @JoinColumn(name = "IDEMPRESA")
    Empresa empresa;

    @Id
    @ManyToOne(optional = false, cascade = CascadeType.MERGE, fetch = FetchType.LAZY)
    @JoinColumn(name = "IDCLIENTE")
    Cliente cliente;

    public Clienteempresa() {
    }

    public Clienteempresa(int idclienteempresa, Empresa empresa, Cliente cliente) {
        this.idclienteempresa = idclienteempresa;
        this.empresa = empresa;
        this.cliente = cliente;
    }

    public int getIdclienteempresa() {
        return idclienteempresa;
    }

    public void setIdclienteempresa(int idclienteempresa) {
        this.idclienteempresa = idclienteempresa;
    }

    public Empresa getEmpresa() {
        return empresa;
    }

    public void setEmpresa(Empresa empresa) {
        this.empresa = empresa;
    }

    public Cliente getCliente() {
        return cliente;
    }

    public void setCliente(Cliente cliente) {
        this.cliente = cliente;
    }

}
