package api.ferreteria.modelo;

import java.io.Serializable;
import javax.enterprise.context.ApplicationScoped;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;

@ApplicationScoped
@Entity(name = "ESTADODETALLEOC")
public class Estadodetalleoc implements Serializable{
    
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "IDESTADODETALLEOC")
    Integer idestadodetalleoc;
    
    @Column(name = "NOMESTADODETALLEOC")
    String nomestadodetalleoc;

    public Estadodetalleoc() {
    }

    public Estadodetalleoc(Integer idestadodetalleoc, String nomestadodetalleoc) {
        this.idestadodetalleoc = idestadodetalleoc;
        this.nomestadodetalleoc = nomestadodetalleoc;
    }

    public Integer getIdestadodetalleoc() {
        return idestadodetalleoc;
    }

    public void setIdestadodetalleoc(Integer idestadodetalleoc) {
        this.idestadodetalleoc = idestadodetalleoc;
    }

    public String getNomestadodetalleoc() {
        return nomestadodetalleoc;
    }

    public void setNomestadodetalleoc(String nomestadodetalleoc) {
        this.nomestadodetalleoc = nomestadodetalleoc;
    }
    
    
    
}
