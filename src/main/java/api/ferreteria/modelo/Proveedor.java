package api.ferreteria.modelo;

import java.io.Serializable;
import javax.enterprise.context.ApplicationScoped;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;

@ApplicationScoped
@Entity(name = "PROVEEDOR")
public class Proveedor implements Serializable{
    
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "IDPROVEEDOR")
    Integer idproveedor;
    
    @Column(name = "NOMPROVEEDOR")
    String nomproveedor;
    
    @Column(name = "CELULAR")
    String celular;
    
    @Column(name = "RUBRO")
    String rubro;

    public Proveedor() {
    }

    public Proveedor(Integer idproveedor, String nomproveedor, String celular, String rubro) {
        this.idproveedor = idproveedor;
        this.nomproveedor = nomproveedor;
        this.celular = celular;
        this.rubro = rubro;
    }

    public Integer getIdproveedor() {
        return idproveedor;
    }

    public void setIdproveedor(Integer idproveedor) {
        this.idproveedor = idproveedor;
    }

    public String getNomproveedor() {
        return nomproveedor;
    }

    public void setNomproveedor(String nomproveedor) {
        this.nomproveedor = nomproveedor;
    }

    public String getCelular() {
        return celular;
    }

    public void setCelular(String celular) {
        this.celular = celular;
    }

    public String getRubro() {
        return rubro;
    }

    public void setRubro(String rubro) {
        this.rubro = rubro;
    }
    
    
}