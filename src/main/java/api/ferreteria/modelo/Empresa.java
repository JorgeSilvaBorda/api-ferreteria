/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package api.ferreteria.modelo;

import java.io.Serializable;
import javax.enterprise.context.ApplicationScoped;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;


@ApplicationScoped
@Entity(name = "EMPRESA")
public class Empresa implements Serializable{
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "IDEMPRESA")
    Integer idempresa;
    
    @Column(name = "RUTEMPRESA")
    String rutempresa;
    
    @Column(name = "NOMEMPRESA")
    String nomempresa;
    
    @Column(name = "RAZONEMPRESA")
    String razonempresa;
    
    @Column(name = "DIRECCIONCOMERCIAL")
    String direccioncomercial;
    
    @Column(name = "GIRO")
    String giro;
    
    @Column(name = "FONO")
    String fono;
    
    @Column(name = "EMAIL")
    String email;

    public Empresa() {
    }

    public Empresa(Integer idempresa, String rutempresa, String nomempresa, String razonempresa, String direccioncomercial, String giro, String fono, String email) {
        this.idempresa = idempresa;
        this.rutempresa = rutempresa;
        this.nomempresa = nomempresa;
        this.razonempresa = razonempresa;
        this.direccioncomercial = direccioncomercial;
        this.giro = giro;
        this.fono = fono;
        this.email = email;
    }

    public Integer getIdempresa() {
        return idempresa;
    }

    public void setIdempresa(Integer idempresa) {
        this.idempresa = idempresa;
    }

    public String getRutempresa() {
        return rutempresa;
    }

    public void setRutempresa(String rutempresa) {
        this.rutempresa = rutempresa;
    }

    public String getNomempresa() {
        return nomempresa;
    }

    public void setNomempresa(String nomempresa) {
        this.nomempresa = nomempresa;
    }

    public String getRazonempresa() {
        return razonempresa;
    }

    public void setRazonempresa(String razonempresa) {
        this.razonempresa = razonempresa;
    }

    public String getDireccioncomercial() {
        return direccioncomercial;
    }

    public void setDireccioncomercial(String direccioncomercial) {
        this.direccioncomercial = direccioncomercial;
    }

    public String getGiro() {
        return giro;
    }

    public void setGiro(String giro) {
        this.giro = giro;
    }

    public String getFono() {
        return fono;
    }

    public void setFono(String fono) {
        this.fono = fono;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }
    
    
    
}
