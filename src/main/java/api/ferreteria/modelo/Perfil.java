/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package api.ferreteria.modelo;

import java.io.Serializable;
import javax.enterprise.context.ApplicationScoped;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;


@ApplicationScoped
@Entity(name = "PERFIL")
public class Perfil implements Serializable{
    
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "IDPERFIL")
    Integer idperfil;
    
    @Column(name = "NOMPERFIL")
    String nomperfil;

    public Perfil() {
    }

    public Perfil(Integer idperfil, String nomperfil) {
        this.idperfil = idperfil;
        this.nomperfil = nomperfil;
    }

    public Integer getIdperfil() {
        return idperfil;
    }

    public void setIdperfil(Integer idperfil) {
        this.idperfil = idperfil;
    }

    public String getNomperfil() {
        return nomperfil;
    }

    public void setNomperfil(String nomperfil) {
        this.nomperfil = nomperfil;
    }
    
    
    
}
