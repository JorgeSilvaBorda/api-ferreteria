/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package api.ferreteria.modelo;

import java.io.Serializable;
import javax.enterprise.context.ApplicationScoped;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;

@ApplicationScoped
@Entity(name = "MONEDA")
public class Moneda implements Serializable{
    
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "IDMONEDA")
    Integer idmoneda;
    
    @Column(name = "NOMMONEDA")
    String nommoneda;
    
    @Column(name = "ACRONIMO")
    String acronimo;

    public Moneda() {
    }

    public Moneda(Integer idmoneda, String nommoneda, String acronimo) {
        this.idmoneda = idmoneda;
        this.nommoneda = nommoneda;
        this.acronimo = acronimo;
    }

    public Integer getIdmoneda() {
        return idmoneda;
    }

    public void setIdmoneda(Integer idmoneda) {
        this.idmoneda = idmoneda;
    }

    public String getNommoneda() {
        return nommoneda;
    }

    public void setNommoneda(String nommoneda) {
        this.nommoneda = nommoneda;
    }

    public String getAcronimo() {
        return acronimo;
    }

    public void setAcronimo(String acronimo) {
        this.acronimo = acronimo;
    }
    
    
    
    

    
    
    
    
}
