
package api.ferreteria.modelo;

import java.io.Serializable;
import javax.enterprise.context.ApplicationScoped;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;


@ApplicationScoped
@Entity(name = "PERFILMODULO")
public class Perfilmodulo implements Serializable{
    
    @Id
    @JoinColumn(name = "PERFIL")
    @Column(name = "IDPERFIL")
    Integer idperfil;
    
    @JoinColumn(name = "MODULO")
    @Column(name = "IDMODULO")
    String idmodulo;
    
}
