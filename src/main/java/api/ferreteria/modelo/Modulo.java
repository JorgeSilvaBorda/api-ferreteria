package api.ferreteria.modelo;

import java.io.Serializable;
import javax.enterprise.context.ApplicationScoped;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;

@ApplicationScoped //Indicación para que las librerías de persistencia incluyan esta clase en el mapeo y validación del modelo de datos de la base de datos.
@Entity(name = "MODULO")
public class Modulo implements Serializable{
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "IDMODULO")
    private int idmodulo;
    
    @Column(name = "NOMMODULO")
    private String nommodulo;

    public Modulo() {
    }

    public Modulo(int idmodulo, String nommodulo) {
        this.idmodulo = idmodulo;
        this.nommodulo = nommodulo;
    }

    public int getIdmodulo() {
        return idmodulo;
    }

    public void setIdmodulo(int idmodulo) {
        this.idmodulo = idmodulo;
    }

    public String getNommodulo() {
        return nommodulo;
    }

    public void setNommodulo(String nommodulo) {
        this.nommodulo = nommodulo;
    }
    
    

    @Override
    public String toString() {
        return "Modulo{" + "IdModulo=" + idmodulo + ", NomModulo=" + nommodulo + '}';
    }
    
}